// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"

#include "RunCharacter.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnDeath);

UCLASS()
class ENDLESS_API ARunCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	ARunCharacter();

	UPROPERTY(BlueprintAssignable)
	FOnDeath OnDeath;

	bool isDead ;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	class UCameraComponent* Camera;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	class USpringArmComponent* SpringArm;
	


	UFUNCTION(BlueprintCallable)
	void Die();

	/*UFUNCTION(BlueprintCallable)
	void onDeath(UPrimitiveComponent* HitComp, AActor* OtherActor,
		UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit);*/
	

	//Coinm 
	UFUNCTION(BlueprintCallable)
	void AddCoin();

	UPROPERTY(BlueprintReadWrite) //BlueprintReadWrite
		int32 TotalCoins;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;


};
