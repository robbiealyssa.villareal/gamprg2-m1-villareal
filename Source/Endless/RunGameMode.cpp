// Fill out your copyright notice in the Description page of Project Settings.


#include "RunGameMode.h"
#include "Tile.h"
#include "RunCharacter.h"
#include "Engine/World.h"    //?
#include "GameFramework/Actor.h"


void ARunGameMode::BeginPlay()
{
	Super::BeginPlay();

	for (int i = 0; i < NumOfTiles; i++)
	{
		AddTile();
	}
	
}

void ARunGameMode::AddTile()    
{
	
	//FActorSpawnParameters SpawnParams; 
	ATile* Tile = GetWorld()->SpawnActor<ATile>(SpawnTile, SpawnPoint);
	//Tile->TileExited.AddDynamic(this, &ARunGameMode::OnTileExited);
	Tile->TileExited.AddDynamic(this, &ARunGameMode::AddTile);
	SpawnPoint = Tile->GetAttachTransform();

	//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, TEXT("Tile Exited"));

}

//void ARunGameMode::OnTileExited() //ATile* Tile
//{
//OnTIleExited->AddTIle->Delay->Destroy Tile
//	AddTile();
//	
//	//GetWorldTimerManager().SetTimer(timer, this, &ATile::DestroyTile, 1.0f, false)
//	
//Destroy Tile
//	
//	tile->Destroy();
//}