// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "EndlessGameMode.generated.h"

UCLASS(minimalapi)
class AEndlessGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AEndlessGameMode();
};



