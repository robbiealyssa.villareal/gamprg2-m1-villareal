// Fill out your copyright notice in the Description page of Project Settings.


#include "RunCharacterController.h"
#include "GameFramework/Actor.h"
#include "RunCharacter.h"


void ARunCharacterController::BeginPlay()
{
	Super::BeginPlay();

	RunCharacter = Cast<ARunCharacter>(GetPawn());
	
}

void ARunCharacterController::SetupInputComponent()
{
	Super::SetupInputComponent();

	InputComponent->BindAxis("MoveRight", this, &ARunCharacterController::MoveRight);
}

void ARunCharacterController::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (!RunCharacter->isDead )
	{
		// Implement Forward movement
		MoveForward(1);
	}

}

void ARunCharacterController::MoveRight(float scale)
{
	FRotator Rotation = this->GetControlRotation();
	FRotator YawRotation(0.0f, Rotation.Yaw, 0.0f);

	FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::Y); //get sideward direction
	
	//RunCharacter->AddMovementInput(Direction, scale);
///
/// 
	//RunCharacter->GetActorRightVector();
	RunCharacter->AddMovementInput(RunCharacter->GetActorRightVector(), scale);
}

void ARunCharacterController::MoveForward(float scale)
{
	FRotator Rotation = this->GetControlRotation();

	FVector Direction = FRotationMatrix(Rotation).GetUnitAxis(EAxis::X);
	RunCharacter->AddMovementInput(Direction, scale);

	
}
