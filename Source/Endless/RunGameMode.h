// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "Tile.h"
#include "RunGameMode.generated.h"

/**
 * 
 */
UCLASS()
class ENDLESS_API ARunGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:


protected:
	virtual void BeginPlay() override;

	UFUNCTION()
	void AddTile();  



	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	AActor* RunCharacter;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	int32 NumOfTiles = 4;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	FTransform SpawnPoint;
	

	UPROPERTY(EditDefaultsOnly, Category = "Spawn Tile")
	TSubclassOf<ATile> SpawnTile;

	UPROPERTY(BlueprintReadOnly)
	ATile* tile;

	//UFUNCTION()
	//void OnTileExited(); //ATile* Tile

private:
	
	
};
