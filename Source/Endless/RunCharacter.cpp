// Fill out your copyright notice in the Description page of Project Settings.

#include "Camera/CameraComponent.h"
#include "GameFramework/SpringArmComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "RunCharacter.h"
#include "Components/BoxComponent.h"
#include "RunCharacterController.h"
#include "Obstacle.h"


// Sets default values
ARunCharacter::ARunCharacter()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	SpringArm = CreateDefaultSubobject<USpringArmComponent>(TEXT("SpringArm"));
	SpringArm->SetupAttachment(RootComponent);

	Camera = CreateDefaultSubobject<UCameraComponent>(TEXT("Camera"));
	Camera->AttachTo(SpringArm);
	
}

// Called when the game starts or when spawned
void ARunCharacter::BeginPlay()
{
	Super::BeginPlay();
	
}

void ARunCharacter::Die()
{

	if (isDead) {
		return;
	}

	isDead = true;

	//Disable input
	Controller->DisableInput(Cast<APlayerController>(GetController()));
	
	//Set visibility skeletalmesh false
	this->GetMesh()->SetVisibility(false);

	//Call OnDeath
	OnDeath.Broadcast();

}

void ARunCharacter::AddCoin()
{
	GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, TEXT("add coin"));
	//TotalCoins += 1;
	TotalCoins += 1;
}

// Called every frame
void ARunCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (isDead == true) {
		Die(); 
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, TEXT("isdead"));
	}

}

