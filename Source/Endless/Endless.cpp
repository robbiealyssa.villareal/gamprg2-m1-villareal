// Copyright Epic Games, Inc. All Rights Reserved.

#include "Endless.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, Endless, "Endless" );
 