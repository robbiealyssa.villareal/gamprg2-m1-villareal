// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "RunCharacter.h"
#include "Components/InputComponent.h"
#include "RunCharacterController.generated.h"

/**
 * 
 */
UCLASS()
class ENDLESS_API ARunCharacterController : public APlayerController
{
	GENERATED_BODY()

public:
	


protected:

	UPROPERTY(BlueprintReadOnly)
	ARunCharacter* RunCharacter;

	virtual void BeginPlay() override;

	virtual void SetupInputComponent() override;

	virtual void Tick(float DeltaTime) override;

	//Input Bindings
	void MoveRight(float scale);
	void MoveForward(float scale);
	

};
