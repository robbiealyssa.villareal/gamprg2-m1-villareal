// Copyright Epic Games, Inc. All Rights Reserved.

#include "EndlessGameMode.h"
#include "EndlessCharacter.h"
#include "UObject/ConstructorHelpers.h"

AEndlessGameMode::AEndlessGameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/ThirdPersonCPP/Blueprints/ThirdPersonCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}
