// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "RunCharacter.h"
#include "Components/SceneComponent.h" //
#include "Components/ArrowComponent.h"  //
#include "Components/BoxComponent.h"  //
#include "Obstacle.h"
#include "Pickup.h"
#include "Tile.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FTileExited);   // OneParam, class ATile*, Exited

UCLASS()
class ENDLESS_API ATile : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ATile();

	UPROPERTY(BlueprintAssignable)
	FTileExited TileExited;   

	UFUNCTION()
	FTransform GetAttachTransform();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;


	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	class USceneComponent* SceneComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	class UArrowComponent* AttachPoint;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	class UBoxComponent* ExitTrigger;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Obstacles Types")
	class UBoxComponent* SpawnPointObstacle;  //Spawn obstacle bounding box; BoundingBox

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "PickUps")
	class UBoxComponent* SpawnPointPickUp;

	UPROPERTY(EditAnywhere, Category = "Obstacle Types")
	TArray<TSubclassOf<class AObstacle>> obstacleTypes;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	int32 ObstacleIndex;

	UPROPERTY(EditAnywhere, Category = "PickUps")
	TArray<TSubclassOf<class APickup>> PickUpItems;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	int32 PickupIndex;



	UPROPERTY(BlueprintReadOnly)
	ARunCharacter* RunCharacter;

	UFUNCTION()
	void OnExit(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, 
		int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	UFUNCTION()
	void DestroyTile();

	//Obstacle
	UPROPERTY(EditDefaultsOnly, Category = "Spawn Obstacle")
	TSubclassOf<class AObstacle> obstacle;

	//Pickup
	UPROPERTY(EditDefaultsOnly, Category = "Spawn Pickup")
	TSubclassOf<class APickup> pickup;

	virtual void Destroyed() override;
	


public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;


private:
	FTimerHandle timer;
	//FTransform SpawnPoint();

	void SpawnObstacle();
	void SpawnPickUp();
	void Spawn();
	float spawnChance = 0.3f;

	TArray<AActor*> SpawnObjs;

};
