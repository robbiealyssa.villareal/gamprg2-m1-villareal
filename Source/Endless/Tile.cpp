// Fill out your copyright notice in the Description page of Project Settings.

#include "Tile.h"
#include "GameFramework/Actor.h"
#include "RunCharacter.h"
#include "Components/SceneComponent.h"    //
#include "TimerManager.h"
#include "Math/UnrealMathUtility.h" //
#include "Kismet/KismetMathLibrary.h"

// Sets default values
ATile::ATile()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	SceneComponent = CreateDefaultSubobject<USceneComponent>(TEXT("SceneComponent"));
	RootComponent = SceneComponent;
	//SetRootComponent(SceneComponent);
	
	AttachPoint = CreateDefaultSubobject<UArrowComponent>(TEXT("ArrowComponent"));
	AttachPoint->SetupAttachment(RootComponent); 

	ExitTrigger = CreateDefaultSubobject<UBoxComponent>(TEXT("BoxComponent"));
	ExitTrigger->SetupAttachment(RootComponent);
	//ExitTrigger->OnComponentBeginOverlap.AddDynamic(this, &ATile::OnExit);

	SpawnPointObstacle = CreateDefaultSubobject<UBoxComponent>(TEXT("SpawnObstacle"));
	SpawnPointObstacle->SetupAttachment(RootComponent);

	SpawnPointPickUp = CreateDefaultSubobject<UBoxComponent>(TEXT("SpawnPickup"));
	SpawnPointPickUp->SetupAttachment(RootComponent);
}

// Called when the game starts or when spawned
void ATile::BeginPlay()
{
	Super::BeginPlay();

	ExitTrigger->OnComponentBeginOverlap.AddDynamic(this, &ATile::OnExit);
	
	//SpawnObstacle();
	Spawn();
	
}

FTransform ATile::GetAttachTransform()
{ 
	return FTransform(AttachPoint->GetComponentTransform());  //GetComponentLocation
}

void ATile::OnExit(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	
	if (ARunCharacter* runCharacter = Cast<ARunCharacter>(OtherActor))    
	{
		TileExited.Broadcast(); //this,tile
	
		GetWorld()->GetTimerManager().SetTimer(timer, this, &ATile::DestroyTile, 1.0f, false);
	}

}

void ATile::DestroyTile()
{
	Destroy();
}

void ATile::Destroyed()
{
	for (AActor* objectSpawned : SpawnObjs) {
		objectSpawned->Destroy();
	}
}

// Called every frame
void ATile::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void ATile::SpawnObstacle()
{
	//const FVector& origin = SpawnPointObstacle->GetComponentLocation();
	const FVector& origin = SpawnPointObstacle->Bounds.Origin;
	//const FVector& boxExtent = SpawnPointObstacle->GetUnscaledBoxExtent();
	const FVector& boxExtent = SpawnPointObstacle->Bounds.BoxExtent;

	//UKismetMathLibrary::RandomPointInBoundingBox(const FVector& Origin, const FVector& BoxExtent);

	FVector spawnPoint = UKismetMathLibrary::RandomPointInBoundingBox(origin, boxExtent);
	ObstacleIndex = UKismetMathLibrary::RandomIntegerInRange(0, obstacleTypes.Num() - 1);
	
	AObstacle* spawnObstacle = GetWorld()->SpawnActor<AObstacle>(obstacleTypes[ObstacleIndex], FTransform(spawnPoint));
	spawnObstacle->AttachToActor(this, FAttachmentTransformRules::KeepWorldTransform);

	//add to SpawnObjs -> destroy
	SpawnObjs.Add(spawnObstacle);
}

void ATile::SpawnPickUp()
{
	const FVector& Origin = SpawnPointPickUp->Bounds.Origin;
	const FVector& BoxExtent = SpawnPointPickUp->Bounds.BoxExtent;

	FVector SpawnPoint = UKismetMathLibrary::RandomPointInBoundingBox(Origin, BoxExtent);
	PickupIndex = UKismetMathLibrary::RandomIntegerInRange(0, PickUpItems.Num() - 1);

	APickup* spawnPickup = GetWorld()->SpawnActor<APickup>(PickUpItems[PickupIndex], FTransform(SpawnPoint));
	spawnPickup->AttachToActor(this, FAttachmentTransformRules::KeepWorldTransform);

	SpawnObjs.Add(spawnPickup);
}

void ATile::Spawn()
{
	SpawnObstacle();

	//spawn randomboolwithweight
	/*if (UKismetMathLibrary::RandomBoolWithWeight(spawnChance)) {
		SpawnObstacle();
	}*/

	//spawn pick up
	SpawnPickUp();
}

